package fr.sdachess.hangmanPlugin.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.sdachess.hangmanPlugin.Main;
import fr.sdachess.hangmanPlugin.objects.Game;

public class CommandHangman implements CommandExecutor{
    
    public Main instance = Main.getInstance();
    
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player) {
			Player p = (Player)sender;
			if(args.length == 1) {
				if(args[0].equalsIgnoreCase("stop")) {
					if(instance.gamemanager.hasGame(p)) {
						instance.games.get(p).finish(false);
					}else {
						p.sendMessage(Main.prefix+"&c Vous n'avez aucune partie en cours !");
					}
				}else {
					showhelp(p);
				}
				return true;
			}
			if(instance.gamemanager.hasGame(p)) {
				p.sendMessage(Main.prefix+"&c Vous avez déjà une partie en cours !");
				return false;
			}
		    sender.sendMessage("Vous avez lancé le jeu du pendu !");
		    
		    Game game = new Game(p);
		    game.start();
		    instance.games.put(p, game);
		    
		}
	
        return true;
    }
    
    public void showhelp(Player p) {
    	p.sendMessage("C'est un jeu du pendu simple, le but est de deviner le mot en proposant des lettres dans le chat en moins de 8 coups");
    	p.sendMessage("Pour arrêter une partie en cours :" + Main.prefix + "Utilisation : /pendu [stop]");
    }

}
