package fr.sdachess.hangmanPlugin.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import fr.sdachess.hangmanPlugin.Main;

public class ChatListener implements Listener {
    
   public Main instance = Main.getInstance();
   
   @EventHandler
   public void onchat(AsyncPlayerChatEvent e) {
	   Player p = e.getPlayer();
	   if(instance.gamemanager.hasGame(p)) {
		   e.setCancelled(true);
		   if(e.getMessage().length() != 1) {
			   p.sendMessage(Main.prefix+"&cVous ne pouvez donner qu'un seul caractère !");
			   return;
		   }
		   instance.games.get(p).initChar(e.getMessage());
		   if(instance.gamemanager.hasGame(p)) {
			   if(instance.games.get(p).started) {
				   p.sendMessage(Main.prefix+"&a Le mot : &2"+instance.games.get(p).displayedWord);
			   }
		   }
	   }
   }
    

}
