package fr.sdachess.hangmanPlugin.objects;

import java.util.ArrayList;
import java.util.Random;

import org.bukkit.entity.Player;

import fr.sdachess.hangmanPlugin.Main;

public class Game {
	
	public Main instance = Main.getInstance();
	
	private String[] words = {"vocal", "couteau", "aeroport", "vache", "terrain", "minecraft", "developpement", "java", "cafe", "anticonstitutionnellement", "donation"};
	private ArrayList<String> CharsFounds = new ArrayList<>();
	public String word;
	public String displayedWord;
	public int essais = 0;
	public Player p;
	public boolean started = false;
	
	public Game(Player p) {
		this.p = p;
	}
	
	public void start() {
		p.sendMessage(Main.prefix+"&eVous venez de lancer une partie !");
		started = true;
		ChoiceWords();
	}
	
	public void initChar(String char1) {
		if(CharsFounds.contains(char1.toLowerCase()) || CharsFounds.contains(char1.toUpperCase())) {
			p.sendMessage(Main.prefix+"&cVous avez déjà trouvé cette lettre !");
			return;
		}
		if(word.toLowerCase().contains(char1.toLowerCase())) {
			CharsFounds.add(char1);
			p.sendMessage(Main.prefix+"&6Vous avez trouvé la lettre "+char1+" !");
			
			refreshDisplayWord();
			CheckFinish();
			if(started) p.sendMessage(Main.prefix+"&aIl vous reste "+(7-essais)+" essais !");
		}else {
			p.sendMessage(Main.prefix+"&c Le mot ne contient pas cette lettre");
			essais++;
			if(essais != 7) {
				p.sendMessage(Main.prefix+"&aIl vous reste "+(7-essais)+" essais !");
			}else {
				p.sendMessage(Main.prefix+"&cVous n'avez plus d'essais !");
				finish(false);
			}
		}
	}
	
	public void CheckFinish() {
		if(displayedWord.equalsIgnoreCase(word)) {
			finish(true);
		}
	}
	
	public void refreshDisplayWord() {
		String str="";
		for(int i = 0; i < word.length(); i++) {
			String schar = word.charAt(i)+"";
			if(CharsFounds.contains(schar.toLowerCase()) || CharsFounds.contains(schar.toUpperCase())) {
				str = str+schar;
			}else {
				str = str+"*";
			}
		}
		displayedWord = str;
	}	
	
	public void ChoiceWords() {
		Random random = new Random();
		int i = random.nextInt(words.length-1);
		word = words[i];
		refreshDisplayWord();
	}
	
	public void finish(boolean winned) {
		if(winned) {
			p.sendMessage(Main.prefix+"&2&lFélicitations ! &aLe mot était : al"+word);
		}else {
			p.sendMessage(Main.prefix+"&c&lPartie Terminée ! Le mot était :& l"+word);
		}
		
		started = false;
		instance.games.put(p, null);
	}
	
}
