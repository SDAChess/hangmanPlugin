package fr.sdachess.hangmanPlugin;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import fr.sdachess.hangmanPlugin.commands.CommandHangman;
import fr.sdachess.hangmanPlugin.listeners.ChatListener;
import fr.sdachess.hangmanPlugin.managers.GameManager;
import fr.sdachess.hangmanPlugin.objects.Game;

public class Main extends JavaPlugin {

	public static Main instance;
	
	public static final String prefix = "&8[&6&lPendu&8] ";
	
	public GameManager gamemanager;
	
	public HashMap<Player, Game> games = new HashMap<>();
	
	
	public static Main getInstance() {
		return instance;
	}
    
    
    public void onEnable() {
    	instance = this;
    	initValues();
    	initMethods();
        getCommand("pendu").setExecutor(new CommandHangman());
        Bukkit.getPluginManager().registerEvents(new ChatListener(), this);
    }
   
    public void onDisable() {

    }
    
    public void initMethods(){
    	gamemanager = new GameManager();
    }
    public void initValues(){}
}

